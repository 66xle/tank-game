﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using MathClasses;
using static Raylib.Raylib;


namespace Project2D
{
    public class GameObject
    {
        public Vector2 position;
        public Vector2 pivot;
        public float rotation;

        public Matrix4 localTransform;
        public Matrix4 worldTransform;

        public List<GameObject> children;
        public GameObject parent;

        private Texture2D image;

        public GameObject(float xPos, float yPos, float rotation, Texture2D image)
        {
            position = new Vector2(xPos, yPos);
            this.rotation = rotation;
            this.image = image;
            children = new List<GameObject>();
            localTransform = new Matrix4();
            worldTransform = new Matrix4();
        }

        public void AddChild(GameObject child)
        {
            children.Add(child);
            child.parent = this;
        }

        public void SetPivot(float x, float y)
        {
            pivot.x = x;
            pivot.y = y;
        }

        public void Update()
        {
            Matrix4 rotationMatrix = CreateRotationAboutPoint(pivot.x, pivot.y, rotation);
            Matrix4 translationMatrix = new Matrix4();
            translationMatrix.CreateTranslation(position.x, position.y, 0);
            localTransform = translationMatrix * rotationMatrix;

            if (parent != null)
            {
                worldTransform = parent.worldTransform * localTransform;
            }
            else
            {
                worldTransform = localTransform;
            }

            foreach (GameObject child in children)
            {
                child.Update();
            }

        }
        public void Draw()
        {
            // Draw Images
            Game.DrawTextureTransformed(image, worldTransform);
            foreach (GameObject child in children)
            {
                child.Draw();
            }
        }

        // Rotate at Origin
        public static Matrix4 CreateRotationAboutPoint(float xPivot, float yPivot, float rotation)
        {
            Matrix4 pivotToOrigin = new Matrix4();
            Matrix4 originToPivot = new Matrix4();
            Matrix4 rotationMatrix = new Matrix4();

            rotationMatrix.SetRotateZ(rotation);

            pivotToOrigin.CreateTranslation(-xPivot, -yPivot, 0);
            originToPivot.CreateTranslation(xPivot, yPivot, 0);

            return originToPivot * rotationMatrix * pivotToOrigin;
        }
    }
}
