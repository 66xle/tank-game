﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using MathClasses;
using static Raylib.Raylib;

namespace Project2D
{
    class Game
    {
        /// <summary>
        /// This function acts as a middleman of sorts, to translate your transform matrix
        /// into parameters for Raylib's built in DrawTexture function.
        /// It will pull the rotation, the scale, and the translation out of the matrix you
        /// pass in, and use that to draw the sprite in question.
        /// Due to (as far as I can tell) limitations on Raylib, this won't behave correctly
        /// for all possible matrices - it won't handle shear, for instance - but it should
        /// be okay for the basic stuff you can create with your matrix class.
        /// </summary>
        /// <param name="tex">The texture to draw.</param>
        /// <param name="transform">The transform to apply to the texture</param>
        public static void DrawTextureTransformed(Texture2D tex, Matrix4 transform)
        {

            Vector2 xAxis = new Vector2(transform.m1, transform.m2);
            Vector2 yAxis = new Vector2(transform.m5, transform.m6);
            Vector2 translation = new Vector2(transform.m13, transform.m14);
            //Note - the angle of a unit circle count up when they go anticlockwise,
            //but because in Raylib land positive Y is down, a positive angle will
            //be clockwise.
            float angle = (float)Math.Atan2(xAxis.y, xAxis.x);
            float scale = (xAxis.Length() + yAxis.Length()) / 2.0f;

            //We multiply the angle by 180/pi because the matrix library for the project
            //works in radians (that's what the unit tests require) but RayLib expects degrees.
            DrawTextureEx(tex, translation, angle * 180.0f / (float)Math.PI, scale, Color.WHITE);

        }

        Stopwatch stopwatch = new Stopwatch();

        private long currentTime = 0;
        private long lastTime = 0;
        private float timer = 0;
        private int fps = 1;
        private int frames;

        private float deltaTime = 0.005f;

        Texture2D tankImage;
        Texture2D turretImage;

        GameObject tank;
        GameObject turret;

        float turretAngle;
        List<Bullet> bullets;

        public Game()
        {
        }

        public void Init()
        {
            stopwatch.Start();
            lastTime = stopwatch.ElapsedMilliseconds;

            if (Stopwatch.IsHighResolution)
            {
                Console.WriteLine("Stopwatch high-resolution frequency: {0} ticks per second", Stopwatch.Frequency);
            }

            // Loading Images
            tankImage = LoadTextureFromImage(LoadImage("../Images/tankBlack.png"));
            turretImage = LoadTextureFromImage(LoadImage("../Images/barrelBlack.png"));

            SetTargetFPS(60);

            // Bullets
            bullets = new List<Bullet>();

            // Set Tank/Turret Position
            tank = new GameObject(100, 100, 0, tankImage);
            turret = new GameObject(37 - 7, 37 - 10, 0, turretImage);

            // Set Rotation Position
            tank.SetPivot(37, 37);
            turret.SetPivot(7, 10);

            tank.AddChild(turret);
        }

        public void Shutdown()
        {
        }

        public void Update()
        {
            lastTime = currentTime;
            currentTime = stopwatch.ElapsedMilliseconds;
            deltaTime = (currentTime - lastTime) / 1000.0f;
            timer += deltaTime;
            if (timer >= 1)
            {
                fps = frames;
                frames = 0;
                timer -= 1;
            }
            frames++;

            float tankRotationSpeed = 5.0f;
            float tankSpeed = 100;
            float turretRotationSpeed = 5.0f;

            MathClasses.Vector4 tankVelocity = new MathClasses.Vector4(tankSpeed, 0, 0, 0);
            tankVelocity = tank.worldTransform * tankVelocity;

            // Movement of Tank
            if (IsKeyDown(KeyboardKey.KEY_W))
            {
                tank.position.x += tankVelocity.x * deltaTime;
                tank.position.y += tankVelocity.y * deltaTime;
            }
            if (IsKeyDown(KeyboardKey.KEY_S))
            {
                tank.position.x -= tankVelocity.x * deltaTime;
                tank.position.y -= tankVelocity.y * deltaTime;
            }

            // Tank Rotation
            if (IsKeyDown(KeyboardKey.KEY_A))
            {
                tank.rotation -= tankRotationSpeed * deltaTime;
            }
            if (IsKeyDown(KeyboardKey.KEY_D))
            {
                tank.rotation += tankRotationSpeed * deltaTime;
            }

            // Turret Rotation
            if (IsKeyDown(KeyboardKey.KEY_Q))
            {
                turret.rotation -= turretRotationSpeed * deltaTime;
            }
            if (IsKeyDown(KeyboardKey.KEY_E))
            {
                turret.rotation += turretRotationSpeed * deltaTime;
            }

            // Shoot Bullet
            if (IsKeyPressed(KeyboardKey.KEY_SPACE))
            {
                Matrix4 translation = new Matrix4();
                translation.m13 = 45;
                translation.m14 = 8;
                translation.m15 = 0;

                Matrix4 localTransform = turret.worldTransform * translation;
                Vector2 position = new Vector2(localTransform.m13, localTransform.m14);

                Vector2 velocity = new Vector2((float)Math.Cos(tank.rotation + turret.rotation), (float)Math.Sin(tank.rotation + turret.rotation));
                SpawnBullet(position, velocity);
            }

            // Bullet Travels
            foreach (Bullet bullet in bullets)
            {
                bullet.KinematicUpdate();
            }

            // Checks if Bullets is off Screen and Deletes
            for (int index = 0; index < bullets.Count; index++)
            {
                Bullet bullet = bullets[index];
                if (bullet.position.x > GetScreenWidth() || bullet.position.x < 0 || bullet.position.y > GetScreenHeight() || bullet.position.y < 0)
                {
                    bullets.Remove(bullet);
                }
            }

            // Updates Takes Movement
            tank.Update();
        }

        // Spawns Bullet in Position
        public void SpawnBullet(Vector2 position, Vector2 velocity)
        {
            Bullet newBullet = new Bullet(position, 5);
            newBullet.velocity = velocity;
            bullets.Add(newBullet);
        }

        public void Draw()
        {
            BeginDrawing();

            ClearBackground(Color.WHITE);

            // Draws Tank
            tank.Draw();

            // Draw Bullets
            foreach(Bullet thisBullet in bullets)
            {
                thisBullet.DrawBullet();
            }

            EndDrawing();
        }

    }
}
