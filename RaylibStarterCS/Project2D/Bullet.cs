﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using MathClasses;
using static Raylib.Raylib;

namespace Project2D
{
    class Bullet
    {
        public float radius;
        public Vector2 position;
        public Vector2 velocity;
        public Vector2 acceleration;

        public Bullet(float x, float y, float radius)
        {
            position = new Vector2(x, y);
            this.radius = radius;
            acceleration = new Vector2();
        }

        public Bullet(Vector2 position, float radius)
        {
            this.position = position;
            this.radius = radius;
            acceleration = new Vector2();
        }

        // Bullet Travel Speed
        public void KinematicUpdate()
        {
            position += velocity * 10;
            velocity += acceleration * 10;
        }

        // Draws Circle on Screen
        public void DrawBullet()
        {
            DrawCircle((int)position.x, (int)position.y, radius, Color.RED);
        }
    }
}
