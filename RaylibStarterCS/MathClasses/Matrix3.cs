﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathClasses
{
    public class Matrix3
    {
        public float m1, m2, m3, m4, m5, m6, m7, m8, m9;

        public Matrix3()
        {

        }

        public Matrix3(float m1, float m2, float m3, float m4, float m5, float m6, float m7, float m8, float m9)
        {
            this.m1 = m1; this.m2 = m2; this.m3 = m3;
            this.m4 = m4; this.m5 = m5; this.m6 = m6;
            this.m7 = m7; this.m8 = m8; this.m9 = m9;
        }

        public void SetRotateX(double radians)
        {
            m1 = 1; m2 = 0; m3 = 0;
            m4 = 0; m5 = (float)Math.Cos(radians); m6 = (float)Math.Sin(radians);
            m7 = 0; m8 = (float)-Math.Sin(radians); m9 = (float)Math.Cos(radians);
        }
        public void SetRotateY(double radians)
        {
            m1 = (float)Math.Cos(radians); m2 = 0; m3 = (float)-Math.Sin(radians);
            m4 = 0; m5 = 1; m6 = 0;
            m7 = (float)Math.Sin(radians); m8 = 0; m9 = (float)Math.Cos(radians);

        }
        public void SetRotateZ(double radians)
        {
            m1 = (float)Math.Cos(radians); m2 = (float)Math.Sin(radians); m3 = 0;
            m4 = (float)-Math.Sin(radians); m5 = (float)Math.Cos(radians); m6 = 0;
            m7 = 0; m8 = 0; m9 = 1;
        }

        public static Matrix3 operator *(Matrix3 rhs, Matrix3 lhs)
        {
            return new Matrix3(
                (lhs.m1 * rhs.m1)  +  (lhs.m2 * rhs.m4)  +  (lhs.m3 * rhs.m7),     (lhs.m1 * rhs.m2)  +  (lhs.m2 * rhs.m5)  +  (lhs.m3 * rhs.m8),    (lhs.m1 * rhs.m3)  +  (lhs.m2 * rhs.m6)  +  (lhs.m3 * rhs.m9),
                (lhs.m4 * rhs.m1)  +  (lhs.m5 * rhs.m4)  +  (lhs.m6 * rhs.m7),     (lhs.m4 * rhs.m2)  +  (lhs.m5 * rhs.m5)  +  (lhs.m6 * rhs.m8),    (lhs.m4 * rhs.m3)  +  (lhs.m5 * rhs.m6)  +  (lhs.m6 * rhs.m9),
                (lhs.m7 * rhs.m1)  +  (lhs.m8 * rhs.m4)  +  (lhs.m9 * rhs.m7),     (lhs.m7 * rhs.m2)  +  (lhs.m8 * rhs.m5)  +  (lhs.m9 * rhs.m8),    (lhs.m7 * rhs.m3)  +  (lhs.m8 * rhs.m6)  +  (lhs.m9 * rhs.m9));
        }

    }
}